# Семинар 13: Оптимизации

В этом семинаре мы изучим некоторые оптимизации, которые производят `gcc` и другие компиляторы языка С.

## 1. Инструменты

Научитесь пользоваться  http://godbolt.org для компиляции C и просмотра ассемблерного кода.

---

**Вопрос** Как посмотреть флаги компиляции в godbolt?

![Ответ](godbolt/compiler_options.png)

---

**Вопрос** Что означают флаги: `-O0`, `-O1`, `-O2`, `-Os`?

```sh
$ gcc --help=optimizers | grep -E "\-O(<number>|s)" -
    -O<number>  Set optimization level to <number>.
    -Os         Optimize for space rather than speed.
$ man --pager=cat gcc | grep -A 100 "Options That Control Optimization" -
    ...
```

---

**Вопрос** Скомпилируйте программу [`print0.c`](printer0.c) из семинара 11 с ключами `-O0` и `-O3` (в Godbolt можно создать две вкладки с компиляторами рядом и сравнить листинги).

![Ответ](godbolt/printer0_compiled.png)

---

## 2. Volatile

Если добавить к типу модификатор `volatile`, то действия с такими данными гарантированно будут произведены.
Если из переменной в коде есть чтение, то чтение обязательно случится, а если будет запись — произойдёт и она.
Для остальных переменных это неверно: например, чтения переменных из памяти не обязательно произойдут, например, зачем читать из памяти константу если можно напрямую подставить её значение?

---

**Вопрос** Покажите как с увеличением уровня оптимизаций пропадают чтения из памяти ([код](volatile.c)):

![Ответ](godbolt/rw_optimizations.png)

---

**Вопрос** пометьте функцию `print_int` как `static`. Что произошло в оптимизированном коде и почему?

![a](godbolt/static_inlining.png)

**Ответ** функция `print_int` исчезла из объектного файла.

> ### C17 standard, 6.2.2 Linkages of identifiers:
>
> An identifier declared in different scopes or in the same scope more than once can be made to refer to the same object or function by a process called **linkage**.
>
> There are three kinds of linkage: external, internal, and none.
>
> In the set of translation units and libraries that constitutes an entire program, each declaration of a particular identifier with **external linkage** denotes the same object or function. 
> 
> Within one translation unit, each declaration of an identifier with **internal linkage** denotes the same object or function. 
> 
> Each declaration of an identifier with no linkage denotes a **unique entity**.
>
> If the declaration of a file scope identifier for an object or a function contains the storage-class specifier `static`, the identifier has **internal linkage**

---

## 3. Пролог и эпилог

При трансляции функций нередко используются понятия *пролога* и *эпилога* — шаблонных фрагментов ассемблерного кода в начале и конце функции.
Их цель — установка регистра `rbp` так, чтобы он указывал на начало стекового фрейма, где лежат все интересные для функции данные.
Пролог устанавливает его, как правило следующей последовательностью инструкций:

```asm
push rbp
mov rbp, rsp
sub rsp, <сколько выделить байтов в стеке>
```

Эпилог возвращает `rbp` в исходное состояние. Напоминаем, что `rbp` — *callee-saved* регистр.

```asm
mov rsp, rbp  ; отмотать стек в исходное состояние
pop rbp       ; восстановить rbp
ret
```
Иногда используется также специализированная инструкция `leave`.

---

**Вопрос** Что делает эта инструкция?

**Ответ**

> ### Intel 64 Instruction Set Reference, LEAVE — High Level Procedure Exit
>
> Releases the stack frame set up by an earlier ENTER instruction. The LEAVE instruction copies the frame pointer (in 
the EBP register) into the stack pointer register (ESP), which releases the stack space allocated to the stack frame. 
The old frame pointer (the frame pointer for the calling procedure that was saved by the ENTER instruction) is then 
popped from the stack into the EBP register, restoring the calling procedure’s stack frame.

---

## 3.1 Red zone

Указатель на вершину стека `rsp` делит память на две части: в старших адресах лежит сам стек, в младшие он растёт.
Мы привыкли к тому, что нельзя обращаться к младшей части памяти  и что-то там хранить.
Однако соглашение вызова на самом деле допускает использование части "рядом" со стеком, а именно 128 байт от `rsp` в область младших адресов:

```
|        |
|  ...   |
|        |
├--------┤ ← rsp-128
|        |
| red    |
| zone   |
|        |
├---^----┤ ← rsp
| stack  |
|        |
```
Чтобы использовать red zone необходимо, чтобы функция не вызывала другие функции.

Если функция не вызывает другие функции и выделяет N байт для хранения данных в стеке, то хранить она может на самом деле N+128 байт.

---

Рассмотрим [следующий файл](prologue-epilogue.c).

**Вопрос** Скомпилируйте его без оптимизаций и объясните содержимое функции `maximum`. Почему `rsp` уменьшается на это число?

![Ответ](godbolt/maximum.png)

**Ответ** 4096 (`char buffer[]`) + 4 (`int a`) + 4 (`int b`) < 3992 + 128 (**red zone**)   

---

## 4. Предподсчёт значений

Скомпилируйте [следующий код](precompute.c) с максимальным уровнем оптимизации и объясните, откуда там берётся строка для `printf`.

![Ответ](godbolt/precomputed.png)
```md
| Строка | Представление в LE   | Память     |
| ------ | -------------------- | ---------- |
| 'hell' | ['l', 'l', 'e', 'h'] | 0x6c6c6568 |
| 'o'    | ['o']                | 0x6f       |
| ' wor' | ['r', 'o', 'w', ' '] | 0x726f7720 |
| 'ld'   | ['d', 'l']           | 0x646c     |
```
---

## 5. Хвостовая рекурсия

**Вопрос** Вспомните, почему пару инструкций `call` + `ret` можно заменить на `jmp`, например:
```asm
    ...
    call f
    ret
f:  ...
    ret

; то же, что и:

    ...
    jmp f
f:  ...
    ret
```

Скомпилируйте и запустите [следующий код](tail-rec.c).
Что выведется на экран? Объясните это поведение.

Как можно переписать функцию (какую?) чтобы программа корректно считала длину длинного списка?

**Ответ**
```sh
$ gcc -O0 -o tail-rec tail-rec.c && ./tail-rec
[1]    332914 segmentation fault (core dumped)  ./tail-rec
$ gcc -O3 -o tail-rec tail-rec.c && ./tail-rec
1048576
```
Неоптимизированная рекурсивная имплементация `list_length` требует количество памяти на стеке, пропорциональное длинне массива. Когда этот размер превышает некое критическое значение, ОС завершает выполнение программы с ошибкой.

```c
size_t list_length(struct list const *l) {
    size_t length = 0;
    while (l) {
        length++;
        l = l->next;
    }
    return length;
}
```
```sh
$ gcc -o tail-rec+ -O0 tail-rec+.c && ./tail-rec+
1048576%                                                           
```

---

## 6. Copy elision

Скомпилируйте [следующий код](return-value.c) с максимальным уровнем оптимизации.

![](godbolt/copy-elision.png)

Объясните, зачем в функцию `sb_init` передаётся аргумент, хотя по сигнатуре у неё параметров нет.

> Copy elision optimization позволяет избежать ненужного копирования значений; аргумент функции - это указатель на `string buffer`, который инициализирует `sb_init`. 

---

## 7. Restrict

Скомпилируйте [следующий код](restrict-0.c) с максимальным уровнем оптимизации.

![](godbolt/restrict-off.png)

Эта функция прибавляет к первому аргументу второй два раза; оба аргумента являются указателями на числа.
Мы могли бы прибавить к первому аргументу удвоенное значение второго и это было бы быстрее.

Посмотрите внимательно на ассемблерную функцию; есть ли там эта оптимизация? Если да, то почему она верна, если нет, то почему неверна?

> Нет; неверна, потому что указатели могут ссылаться на один и тот же элемент.

Модифицируйте код [следующим образом](restrict-1.c).

![](godbolt/restrict-on.png)

Как изменится скомпилированный код с оптимизациями?

> Количество операций чтения/записи уменьшилось до ожидаемого уровня.

Прочитайте стр. 281--282 в "Low-level programming" смысл ключевого слова `restrict` и объясните его влияние на код.

> ### C17 standard, 6.7.3 Type qualifiers:
> 
> An object that is accessed through a `restrict`-qualified pointer has a special association with that pointer. This association, ..., requires that all accesses to that object use, directly or indirectly, the value of that particular pointer.